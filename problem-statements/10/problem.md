### Problem Statement 10
 
There is a coin with denomination of N (an integer).

	
Navodit, being a magician, produces out of thin air, coins of denomination N/2, N/3 and N/4 where each coin has an integral denomination. Coins of non-integral denomination cannot be produced even though Navodit is such a great magician. 


Also, due to his great magic skills, for each coin that appears, each, in turn, produces further coins following the same pattern.


Given a coin with denomination N, find the total number of coins that Navodit conjures up.


**Input Format: **

The first line consists of the number of testcases T.
Each test case consists of the number N.


**Output Format:**

For each testcase, print the desired output in a new line.

**Constraints:**


1 <= T <= 10000

1 <= N <= 10<sup>6</sup>


**Sample Input:**
```
3
10
12
1
```
**Sample Output:**
```
2
12
1
```